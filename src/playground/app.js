class IndecisionApp extends React.Component{

    constructor(props){
        super(props);
        this.state={
            options: props.options
        }
        this.handleDeleteOptions = this.handleDeleteOptions.bind(this);
        this.handlePick = this.handlePick.bind(this);
        this.handleAddOption = this.handleAddOption.bind(this);
        this.handleDeleteOption = this.handleDeleteOption.bind(this);
    }

    componentDidMount(){
        try{
            const json = localStorage.getItem('options')
            const options = JSON.parse(json)
            if(options)
                this.setState(()=> ({options}))    
        } catch(e){
            
        }

    }

    componentDidUpdate(prevProps, prevState){
        if(prevState.options.length != this.state.options.length){
            const json = JSON.stringify(this.state.options);
            localStorage.setItem('options', json);
        }
    }

    componentWillUnmount(){
        console.log('component will unmount')
    }

    handleDeleteOptions(){
        this.setState(()=> ({ options: [] }))
    }

    handleDeleteOption(option){
        this.setState((prevState)=> ({
            options: prevState.options.filter(x=> x!=option)
        }))
        console.log('hdo', option)
    }

    handleAddOption(option){
        if(!option)
            return 'Enter valid value to add item'
        if(this.state.options.indexOf(option)> -1)
            return 'This option already exists'

        this.setState(prevState=> ({options: prevState.options.concat([option])}))
    }

    handlePick(){
        const randomNum = Math.floor(Math.random()*this.state.options.length);
        const option = this.state.options[randomNum];
        alert(option);
    }

    render(){
        const title = 'Indecision';
        const subtitle = 'Put your life iin the hands of computer';
        return <div>
            <Header subtitle={subtitle}/>
            <Action 
                hasOptions={this.state.options.length > 0} 
                handlePick={this.handlePick}/> 
            <Options 
                options={this.state.options}
                handleDeleteOptions={this.handleDeleteOptions}
                handleDeleteOption={this.handleDeleteOption}
            />
            <AddOption handleAddOption={this.handleAddOption}/>
        </div>;
    }
}

const Header = (props)=>{
    return <div><h1>{props.title}</h1>
    {props.subtitle && <h2>{props.subtitle}</h2>}
    </div>;
}

Header.defaultProps = {
    title: 'Indecision'
}

const Action = (props)=>{
    return (
        <div> 
            <button onClick={props.handlePick} disabled={!props.hasOptions}>What should I do?</button>
        </div>
    );
}

const Option = (props)=> {
    let o = props.optionText;        
    return (
        <div>
            {o}
            <button 
                onClick={(e)=> props.handleDeleteOption(props.optionText)}

            >Delete</button>
        </div>)
}

const Options = (props)=>{
    return(
        <div>
        <button onClick={props.handleDeleteOptions}>Remove All</button>
        {props.options.length === 0 && <p>Please add an option to get started!</p>}
        {
            props.options.map(o=> <Option key={o} optionText={o} handleDeleteOption={props.handleDeleteOption} />)
            //this.props.options.map(o=> <p key={o}>{o}</p>)
        }
        </div>
    )
}


class AddOption extends React.Component{

    constructor(props){
        super(props)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.state ={
            error: undefined
        }
    }

    handleSubmit(e){
        e.preventDefault();

        const option = e.target.elements.option.value.trim();
        var error = this.props.handleAddOption(option)
        
        this.setState(()=>({error}))

        if(!error){
            e.target.elements.option.value = '' // czysczenie inputa
        }
    }

    render(){
        return <div>
                {this.state.error && <p>{this.state.error}</p>}
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <input type="text" name="option" />
                    <button>Add Option</button>
                </form>
            </div>
    }
}

IndecisionApp.defaultProps = {
    options: []
}

ReactDOM.render(<IndecisionApp />, document.getElementById('app'));
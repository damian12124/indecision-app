class Person{
    constructor(name, age = 0){
        this.name = name;
        this.age = age;
    }
    getGreeting(){
        return `Hello I m ${name}`;
    }

    getDescripion(){
        return `${this.name} is ${this.age} years old.`
    }
}

class Student extends Person{
    constructor(name, age, major){
        super(name, age);
        this.major = major;
    }
    hasMajor(){
        return !!this.major;
    }
    getDescripion(){
        let desc = super.getDescripion();
        if(this.hasMajor()){
            desc += ` Their major is ${this.major}`;
        }
        return desc;
    }
}

let p = new Person();
console.log(p.getGreeting());
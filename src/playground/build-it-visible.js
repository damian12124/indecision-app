
class Visibility extends React.Component{
    constructor(props){
        super(props)

        this.handleShowDetails = this.handleShowDetails.bind(this);
        this.state ={
            visibility: false
        }
    }

    handleShowDetails(){
        this.setState((prevState)=>{
            return {
                visibility: !prevState.visibility
            }
        })
    }

    render(){
        return (
            <div>
                <h1>Visibility Toggle</h1>
                <button onClick={this.handleShowDetails}>Show details</button>
                    {this.state.visibility && <p>Some message</p>}
                
            </div>
        );
    }
}
ReactDOM.render(<Visibility/>, document.getElementById('app'));



// let visibility = false;

// const onShowDetails = (e)=>{
//     visibility = !visibility;
//     render();
// };


// const appRoot = document.getElementById('app');

// const render = ()=> {
//     const template = (
//         <div>
//             <h1>Visibility Toggle</h1>
//             <button onClick={onShowDetails}>Show details</button>
//             {visibility && <p>Some message</p>}
            
//         </div>
//         );
//         ReactDOM.render(template,appRoot);
// }

// render();


console.log('App.js is running');

const app = {
    title: 'Indecision App',
    subtitle: 'Put your life int the hands of a computer',
    options:[]
}

const onFormSubmit = (e)=>{
    e.preventDefault();
    
    const option = e.target.elements.Option.value;
    if(option){
        app.options.push(option);
        e.target.elements.Option.value = '';
        render();
    }
};

const onRemove=(e)=>{
    app.options = [];
    render();
};

const onMakeDecision = ()=>{
    const randomNum = Math.floor(Math.random()*app.options.length);
    const option = app.options[randomNum];
    alert(option);
};

const appRoot = document.getElementById('app');


const render = ()=> {
    const template = (
        <div>
            <h1>{app.title}</h1>
            <p>{app.subtitle}</p>
            <p>{app.options.length > 0 ? 'Here are your options':"No options"}</p>
            <button disabled={app.options.length==0} onClick={onMakeDecision}>What sould I do?</button>
            <button onClick={onRemove}>Remove</button>
            <ol>
                {app.options.map((o)=> { 
                    return <li key={o}>{o}</li>;
                })}
            </ol>
            <form onSubmit={onFormSubmit}>
                <input type="text" name="Option" />
                <button>Add Option</button>
            </form>
        </div>
        );
        ReactDOM.render(template,appRoot);
}

render();